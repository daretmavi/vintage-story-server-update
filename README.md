# Vintage Story server update

Script to update [Vintage Story](https://www.vintagestory.at/) (game)

## Getting started

You only need the script VS_update_server.sh

- Script expects server instalation in subdirectory /server (see example)
- You can change the server directory in script by changing the S_DIR variable

`# Default server directory
S_DIR="server"`

## How to use
Run script with Vintage Story version.

`./VS_update_server.sh <...VERSION..>`

e.g.: `./VS_update_server.sh 1.16.3`

## Function
- copy server.sh (from server dorectory) to script directory (backup)
- download server files from vintage story server
- extract them in the server directory
- copy server.sh back

**Downloaded archive (e.g. vs_server_1.16.3.tar.gz) and server.sh backup are not removed.**

## Waranty
Use on your own risk.
Try on test server bevore use!

## License
GPLv3
