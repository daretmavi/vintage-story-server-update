#!/usr/bin/env bash

# Archive version from commandline
# E.g. "1.16.1"
S_ARCHIVE_VER=${1}

# Archive internet path
S_ARCHIVE_BASE=https://cdn.vintagestory.at/gamefiles/stable/
# Archive name. Default extension is "tar.gz".
S_ARCHIVE_NAME=vs_server_$S_ARCHIVE_VER.tar.gz
# The whole path
S_ARCHIVE="$S_ARCHIVE_BASE""$S_ARCHIVE_NAME"
# echo $S_ARCHIVE

# Default server directory
S_DIR="server"

# Default data directory
# --- not used yet ---
S_DATA="data"

# Backup the server script with config
S_CONFIG=./"$S_DIR"/server.sh
cp "$S_CONFIG" .
CP_ERR="$?"
#echo "$CP_ERR"

if [ $CP_ERR != 0 ]
	then
		# file copy error
		echo -e "\033[0;31m"
		echo "Error by server config (server.sh) backup"
		echo -e "\033[0m"
		
		read -p "Do you wish to continue upgrading? (y/n)" -n 1 -r
		echo
		if [[ ! $REPLY =~ ^[Yy]$ ]]
		then
			exit 1
		fi
		
		echo "Continue upgrading. Check your server.sh"
		echo
		read -p "Press any key to resume ..."
fi


# Download the archive
wget -c "$S_ARCHIVE"
WGET_ERR="$?"
#echo "$WGET_ERR"

# Check if wgat was successfull
if [ $WGET_ERR -eq 0 ]
	then
		rm -rf ./"$S_DIR"/*		# remove old server files
		# extract new server files
		if tar -zxvf "$S_ARCHIVE_NAME" -C ./"$S_DIR"; then
			cp server.sh ./"$S_DIR"
			echo -e "\033[0;32m"
			echo "Server ist successfully updated to version $S_ARCHIVE_VER"
			echo -e "\033[0m"
		else
			# extraction error
			echo -e "\033[0;31m"
			echo "File tar extract ERROR"
			echo -e "\033[0m"
			exit 1
		fi
	else
		# Download error
		echo -e "\033[0;31m"
		echo "File donwload ERROR"
		echo -e "Check the file name and Path: \033[0;34m$S_ARCHIVE"
		echo -e "\033[0m"
		exit 1
fi

exit 0
